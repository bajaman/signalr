import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HubConnection } from '@aspnet/signalr';

import { jqxGaugeComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgauge';
import { jqxProgressBarComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxprogressbar';
import { jqxNotificationComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
    // UI handles for widgets and other elements
    @ViewChild('notification') notification: jqxNotificationComponent;

    @ViewChild('temp1') temp1: jqxGaugeComponent;
    @ViewChild('temp2') temp2: jqxGaugeComponent;
    @ViewChild('temp3') temp3: jqxGaugeComponent;
    @ViewChild('temp4') temp4: jqxGaugeComponent;
    @ViewChild('temp5') temp5: jqxGaugeComponent;
    @ViewChild('temp6') temp6: jqxGaugeComponent;

    @ViewChild('windows1') windows1: jqxProgressBarComponent;
    @ViewChild('windows2') windows2: jqxProgressBarComponent;
    @ViewChild('windows3') windows3: jqxProgressBarComponent;
    @ViewChild('windows4') windows4: jqxProgressBarComponent;
    @ViewChild('windows5') windows5: jqxProgressBarComponent;
    @ViewChild('windows6') windows6: jqxProgressBarComponent;

    @ViewChild('humid1') humid1: jqxGaugeComponent;
    @ViewChild('humid2') humid2: jqxGaugeComponent;
    @ViewChild('humid3') humid3: jqxGaugeComponent;
    @ViewChild('humid4') humid4: jqxGaugeComponent;
    @ViewChild('humid5') humid5: jqxGaugeComponent;
    @ViewChild('humid6') humid6: jqxGaugeComponent;

    @ViewChild('room1') light1: HTMLDivElement;
    @ViewChild('room2') light2: HTMLDivElement;
    @ViewChild('room3') light3: HTMLDivElement;
    @ViewChild('room4') light4: HTMLDivElement;
    @ViewChild('room5') light5: HTMLDivElement;
    @ViewChild('room6') light6: HTMLDivElement;

    private _hubConnection: HubConnection;
    private _previousGroup: string = 'Any';

    public title = 'SmartHouse';
    public values: number[] = [];
    public maxValue: number = 80;

    public tempSensors: Array<{ name: string, reading: number }> = [];
    public warnings: string[] = [];

    // Sensor group selection options
    public groups: string[] = [
        'Any',
        'Temperature',
        'Humidity',
        'Light',
        'Windows'
    ];

    public temperatureEnabled: boolean = true;
    public humidityEnabled: boolean = true;
    public lightEnabled: boolean = true;
    public windowsEnabled: boolean = true;

    // Called when view is initialized. Creates hub connection, subscribes to events
    // and starts the connection to enable communication.
    ngOnInit() {
        this._hubConnection = new HubConnection('/hubs/SmartHome');
        this.defineMethodsOnProxy();
        this.startConnection();
    }

    private defineMethodsOnProxy() {
        this._hubConnection.on('SensorUpdated', (location: string, name: string, reading: any) => {
            var elem = this[name];
            if (elem) {
                if (name.includes('light')) {
                    if (this.lightEnabled) {
                        elem.nativeElement.className = reading == 'True' ? 'container light-on walls' : 'container light-off walls';
                    }
                }
                else if (name.includes('windows')) {
                    elem.val(reading == 'True' ? 100 : 0);
                }
                else {
                    elem.val(reading);
                }
            }
        });

        this._hubConnection.on('ShowWarning', (warningMessage: string) => {
            let warningSpan = document.getElementById('warning');
            warningSpan.innerText = warningMessage;
            this.notification.open();
            this.warnings.push(warningMessage);
        });
    }

    private startConnection() {
        this._hubConnection.start()
            .then(() => {
                console.log('Hub connection started');
                this._hubConnection.invoke('NotifyReadingsForAllSensors');
                this._hubConnection.invoke('UpdateGroup', this._previousGroup.toLowerCase(), this._previousGroup.toLowerCase());
            })
            .catch(() => {
                console.log('Error while establishing connection');
            });
    }

    // Called when view is destroyed. Stops the hub connection. 
    ngOnDestroy() {
        this.stopConnection();
    }

    private stopConnection() {
        this._hubConnection.stop()
            .then(() => {
                console.log('Hub connection stopped');
            })
            .catch(() => {
                console.log('Error while closing connection');
            });
    }

    // Handler for changing sensor group selection. Sends an UpdateGroup message to hub.
    onChange(group) {
        console.log(group);
        let previousLightEnabled = this.lightEnabled;
        this.temperatureEnabled = group === 'Any' || group === 'Temperature';
        this.humidityEnabled = group === 'Any' || group === 'Humidity';
        this.lightEnabled = group === 'Any' || group === 'Light';
        this.windowsEnabled = group === 'Any' || group === 'Windows';

        if (!this.lightEnabled) {
            this.setLights('container light-disabled walls');
        } else if (!previousLightEnabled) {
            this.setLights('container light-off walls');
        }

        this._hubConnection.invoke('UpdateGroup', group.toLowerCase(), this._previousGroup.toLowerCase());
        this._previousGroup = group;
    }

    setLights(lightDisabledClass) {
        this.setLight(this.light1, lightDisabledClass);
        this.setLight(this.light2, lightDisabledClass);
        this.setLight(this.light3, lightDisabledClass);
        this.setLight(this.light4, lightDisabledClass);
        this.setLight(this.light5, lightDisabledClass);
        this.setLight(this.light6, lightDisabledClass);
    }

    setLight(elem, className) {
        elem.nativeElement.className = className;
    }

    // Humidity widget configurations
    humidMin: any = 10;
    humidMax: any = 90;
    humidTicksMinor: any = { interval: 5, size: '5%' };
    humidTicksMajor: any = { interval: 10, size: '15%' };
    humidLabels: any = { visible: true, position: 'inside' };
    humidStyle: any = { stroke: '#ffffff', 'stroke-width': '1px', fill: '#ffffff' };
    humidCaption: any = { value: 'Humidity', position: 'bottom' };
    humidRanges: any[] =
    [
        { startValue: 10, endValue: 30, style: { fill: '#e2e2e2', stroke: '#e2e2e2' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
        { startValue: 30, endValue: 50, style: { fill: '#f6de54', stroke: '#f6de54' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
        { startValue: 50, endValue: 70, style: { fill: '#db5016', stroke: '#db5016' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
        { startValue: 70, endValue: 90, style: { fill: '#d02841', stroke: '#d02841' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 }
    ];

    // Temperature widget configurations
    tempMin: any = 15;
    tempMax: any = 35;
    tempTicksMinor: any = { interval: 1, size: '5%' };
    tempTicksMajor: any = { interval: 5, size: '15%' };
    tempLabels: any = { visible: true, position: 'inside', interval: 5};
    tempStyle: any = { stroke: '#ffffff', 'stroke-width': '1px', fill: '#ffffff' };
    tempCaption: any = { value: 'Temperature', position: 'bottom' };
    tempRanges: any[] =
    [
        { startValue: 15, endValue: 20, style: { fill: '#e2e2e2', stroke: '#e2e2e2' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
        { startValue: 20, endValue: 25, style: { fill: '#f6de54', stroke: '#f6de54' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
        { startValue: 25, endValue: 30, style: { fill: '#db5016', stroke: '#db5016' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
        { startValue: 30, endValue: 35, style: { fill: '#d02841', stroke: '#d02841' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 }
    ];
}
