using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SmartHome.Models;

namespace SmartHome.Hubs
{
    public class SmartHomeHub : Hub
    {
        private readonly Random _random = new Random();
        private IHouseFacade _houseFacade;

        public SmartHomeHub(IHouseFacade houseFacade)
        {
            _houseFacade = houseFacade;
        }

        public async Task NotifyReadingsForAllSensors()
        {
            foreach (var sensor in _houseFacade.GetAllSensors())
            {
                await Clients.Caller.SendAsync("SensorUpdated", sensor.Location, sensor.Name, sensor.Value.ToString());
            }
        }

        public async Task UpdateGroup(string group, string previousGroup)
        {
            await Groups.RemoveAsync(Context.ConnectionId, previousGroup);
            await Groups.AddAsync(Context.ConnectionId, group);
        }
    }
}
