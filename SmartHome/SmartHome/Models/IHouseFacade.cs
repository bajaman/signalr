using System.Collections;
using System.Collections.Generic;

namespace SmartHome.Models
{
    public interface IHouseFacade
    {
        ISensor GetSensor(string location, string name);
        IEnumerable<ISensor> GetAllSensors();
    }
}
