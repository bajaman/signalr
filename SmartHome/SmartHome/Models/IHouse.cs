using System;
using System.Collections.Generic;

namespace SmartHome.Models
{
    public interface IHouse
    {
        void SetObserver(IHouseObserver observer);

        ISensor GetSensor(string location, string name);

        IEnumerable<ISensor> GetAllSensors();
    }
}
