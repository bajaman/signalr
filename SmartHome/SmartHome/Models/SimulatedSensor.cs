using System;
using System.Threading;
using System.Threading.Tasks;

namespace SmartHome.Models
{
    public class SimulatedSensor : ISensor
    {
        #region ISensor

        private SimulatedSensor()
        {
        }

        public string Location { get; set; }

        public string Name { get; set; }

        public object Value { get; set; }

        public Type Type { get; set; }

        public bool ShowWarning
        {
            get
            {
                switch (Type)
                {
                    case Type.Temperature:
                        return (double)Value > 27;
                    case Type.Humidity:
                        return (double)Value > 70 || (double)Value < 30;
                    case Type.Window:
                        return (bool)Value;
                }
                return false;
            }
        }

        #endregion

        public event EventHandler<SensorEventArgs> SensorUpdated;

        private readonly Random _random = new Random();
        private int _sleepInterval;

        public static SimulatedSensor Create(string location, string name, object initialValue, Type type, int refreshInterval)
        {
            var sensor = new SimulatedSensor()
            {
                Location = location,
                Name = name,
                Value = initialValue,
                Type = type,
                _sleepInterval = refreshInterval,
            };
            Task.Run(sensor.Simulate);
            return sensor;
        }

        private Task Simulate()
        {
            while (true)
            {
                Value = GenerateNewValue();

                SensorUpdated?.Invoke(this, new SensorEventArgs()
                {
                    Location = Location,
                    Name = Name,
                    Value = Value.ToString(),
                    Type = Type,
                    ShowWarning = ShowWarning,
                });

                Thread.Sleep(RandomSleepInterval());
            }
        }

        #region Helpers for random numbers

        private object GenerateNewValue()
        {
            double val;
            switch (Type)
            {
                case Type.Temperature:
                    do
                    {
                        val = RandomNumber(18, 28);
                    }
                    while (Math.Abs(val - (double)Value) > 5.0);
                    return val;
                case Type.Light:
                    return !((bool)Value);
                case Type.Humidity:
                    do
                    {
                        val = RandomNumber(20, 80);
                    }
                    while (Math.Abs(val - (double)Value) > 5.0);
                    return val;
                case Type.Window:
                    return !((bool)Value);
            }
            return null;
        }

        private double RandomNumber(double min, double max)
        {
            var delta = max - min;
            return min + (_random.Next() % ((int)delta * 10)) / 10.0;
        }

        private bool RandomBool()
        {
            return _random.Next() % 2 == 0;
        }

        private int RandomSleepInterval()
        {
            var delta = _random.Next() % _sleepInterval;
            return _sleepInterval + delta;
        }

        #endregion
    }
}
