using System;

namespace SmartHome.Models
{
    public enum Type
    {
        Temperature,
        Humidity,
        Light,
        Window,
    }

    public interface ISensor
    {
        string Location { get; set; }
        string Name { get; }
        object Value { get; }
        Type Type { get; }
        bool ShowWarning { get; }

        event EventHandler<SensorEventArgs> SensorUpdated;
    }
}
