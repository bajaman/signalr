namespace SmartHome.Models
{
    public class SensorEventArgs
    {
        public string Location { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public Type Type { get; set; }
        public bool ShowWarning { get; set; }
    }
}
