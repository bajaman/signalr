using System.Collections.Generic;
using System.Linq;

namespace SmartHome.Models
{
    public class SimulatedHouse : IHouse
    {
        private List<ISensor> _sensors;
        private IHouseObserver _observer;

        private SimulatedHouse()
        {
        }

        private static SimulatedHouse _instance;
        private static readonly object _lock = new object();

        public static SimulatedHouse Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new SimulatedHouse
                            {
                                _sensors = new List<ISensor>
                                {
                                    SimulatedSensor.Create("room1", "temp1", 20.0, Type.Temperature, 1000),
                                    SimulatedSensor.Create("room2", "temp2", 21.0, Type.Temperature, 1000),
                                    SimulatedSensor.Create("room3", "temp3", 22.0, Type.Temperature, 1000),
                                    SimulatedSensor.Create("room4", "temp4", 23.0, Type.Temperature, 1000),
                                    SimulatedSensor.Create("room5", "temp5", 23.0, Type.Temperature, 1000),
                                    SimulatedSensor.Create("room6", "temp6", 23.0, Type.Temperature, 1000),

                                    SimulatedSensor.Create("room1", "humid1", 50.0, Type.Humidity, 2000),
                                    SimulatedSensor.Create("room2", "humid2", 55.0, Type.Humidity, 2000),
                                    SimulatedSensor.Create("room3", "humid3", 50.0, Type.Humidity, 2000),
                                    SimulatedSensor.Create("room4", "humid4", 55.0, Type.Humidity, 2000),
                                    SimulatedSensor.Create("room5", "humid5", 55.0, Type.Humidity, 2000),
                                    SimulatedSensor.Create("room6", "humid6", 55.0, Type.Humidity, 2000),

                                    SimulatedSensor.Create("room1", "light1", false, Type.Light, 4000),
                                    SimulatedSensor.Create("room2", "light2", true, Type.Light, 4000),
                                    SimulatedSensor.Create("room3", "light3", false, Type.Light, 4000),
                                    SimulatedSensor.Create("room4", "light4", true, Type.Light, 4000),
                                    SimulatedSensor.Create("room5", "light5", false, Type.Light, 4000),
                                    SimulatedSensor.Create("room6", "light6", true, Type.Light, 4000),

                                    SimulatedSensor.Create("room1", "windows1", false, Type.Window, 6000),
                                    SimulatedSensor.Create("room2", "windows2", true, Type.Window, 6000),
                                    SimulatedSensor.Create("room3", "windows3", false, Type.Window, 6000),
                                    SimulatedSensor.Create("room4", "windows4", true, Type.Window, 6000),
                                    SimulatedSensor.Create("room5", "windows5", false, Type.Window, 6000),
                                    SimulatedSensor.Create("room6", "windows6", true, Type.Window, 6000),
                                }
                            };

                            foreach (var sensor in _instance._sensors)
                            {
                                sensor.SensorUpdated += _instance.NotifySensorUpdatedAsync;
                            }
                        }
                    }
                }
                return _instance;
            }
        }

        public void SetObserver(IHouseObserver observer)
        {
            _observer = observer;
        }
        private async void NotifySensorUpdatedAsync(object sender, SensorEventArgs args)
        {
            await _observer.NotifySensorChangedAsync(args);
        }

        public ISensor GetSensor(string location, string name)
        {
            return _sensors.FirstOrDefault(s => s.Location == location && s.Name == name);
        }

        public IEnumerable<ISensor> GetAllSensors()
        {
            return _sensors;
        }
    }
}
