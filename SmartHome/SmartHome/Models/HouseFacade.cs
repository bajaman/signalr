using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SmartHome.Hubs;

namespace SmartHome.Models
{
    public class HouseFacade : IHouseFacade, IHouseObserver
    {
        private IHubContext<SmartHomeHub> _hubContext;
        private IHouse _house;

        public HouseFacade(IHubContext<SmartHomeHub> hubContext)
        {
            _hubContext = hubContext;
            _house = SimulatedHouse.Instance;
            _house.SetObserver(this);
        }

        public async Task NotifySensorChangedAsync(SensorEventArgs args)
        {
            var anySensorGroupProxies = _hubContext.Clients.Group("any"); ;
            IClientProxy specificGroupProxies;
            switch (args.Type)
            {
                case Type.Temperature:
                    specificGroupProxies = _hubContext.Clients.Group("temperature");
                    break;
                case Type.Humidity:
                    specificGroupProxies = _hubContext.Clients.Group("humidity");
                    break;
                case Type.Light:
                    specificGroupProxies = _hubContext.Clients.Group("light");
                    break;
                case Type.Window:
                    specificGroupProxies = _hubContext.Clients.Group("windows");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            await anySensorGroupProxies.SendAsync("SensorUpdated", args.Location, args.Name, args.Value);
            await specificGroupProxies.SendAsync("SensorUpdated", args.Location, args.Name, args.Value);

            if (args.ShowWarning && args.Type != Type.Window && args.Type != Type.Light)
            {
                var warningMessage = DateTime.Now + ": Worrying sensor reading: " + args.Location + ":" + args.Name + "  with value " + args.Value;
                await _hubContext.Clients.All.SendAsync("ShowWarning", warningMessage);
            }
        }

        public ISensor GetSensor(string location, string name)
        {
            return _house.GetSensor(location, name);
        }

        public IEnumerable<ISensor> GetAllSensors()
        {
            return _house.GetAllSensors();
        }
    }
}
