webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ".small-height {\r\n    height: 25%;\r\n}\r\n\r\n.large-height {\r\n    height: 75%;\r\n}\r\n\r\n.avg-height {\r\n    height: 50%;\r\n}\r\n\r\n.avg-width {\r\n    width: 50%\r\n}\r\n\r\n.col-height {\r\n    height: 800px;\r\n}\r\n\r\n.row-width {\r\n    width: 1485px;\r\n}\r\n\r\n.light-on {\r\n    background-color: yellow;\r\n}\r\n\r\n.light-off {\r\n    background-color: grey;\r\n}\r\n\r\n.light-disabled {\r\n    background-color: white;\r\n}\r\n\r\n.walls {\r\n    border-width: 3px;\r\n    border-color: black;\r\n    border-style: solid;\r\n}\r\n\r\n.jumbotron {\r\n    height: 175px;\r\n    margin-top: -50px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<jqxNotification #notification\r\n                 [width]=\"'20%'\" [position]=\"'top-right'\" [opacity]=\"0.9\" [autoOpen]=\"false\"\r\n                 [autoClose]=\"true\" [animationOpenDelay]=\"0\" [autoCloseDelay]=\"5000\" [template]=\"'info'\">\r\n    <div>\r\n        <span id=\"warning\" style=\"font-weight: bold;\"></span>.\r\n    </div>\r\n</jqxNotification>\r\n\r\n<div class=\"container\">\r\n    <div class=\"jumbotron\">\r\n        <h1>{{title}}</h1>\r\n        <h5>Notification group: \r\n        <select (change)=\"onChange($event.target.value)\">\r\n            <option *ngFor=\"let group of groups\">{{group}}</option>\r\n        </select>\r\n        </h5>\r\n    </div>\r\n    <div class=\"container\">\r\n        <div class=\"row row-width\">\r\n            <div class=\"col-md-3 border-left border-top border-bottom col-height\">\r\n                <div class=\"row border-bottom avg-height\">\r\n                    <div #room1 class=\"container light-off walls\">\r\n                        <div class=\"row\" style=\"margin-left: 10px\">\r\n                            <p style=\"font-weight: bold\">Guest Bedroom</p>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-2\">\r\n                                <div class=\"row\">\r\n                                    <div style=\"margin-left: 25px; float: left\">\r\n                                        <jqxGauge *ngIf=\"temperatureEnabled === true\" #temp1\r\n                                                  [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                                  [ranges]=\"tempRanges\" [ticksMinor]=\"tempTicksMinor\" [ticksMajor]=\"tempTicksMajor\"\r\n                                                  [labels]=\"tempLabels\" [style]=\"tempStyle\" [caption]=\"tempCaption\" [cap]=\"'radius: 0.04'\"\r\n                                                  [width]=\"150\" [height]=\"150\" [max]=\"tempMax\" [min]=\"tempMin\">\r\n                                        </jqxGauge>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 75px; margin-top: 25px; float: left\">\r\n                                            <p *ngIf=\"windowsEnabled === true\" style=\"font-weight: bold\">Windows opened</p>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 175px; margin-top: 25px; float: left\">\r\n                                            <jqxProgressBar *ngIf=\"windowsEnabled === true\" #windows1\r\n                                                            [animationDuration]=\"0\"\r\n                                                            [width]=\"50\"\r\n                                                            [height]=\"50\"\r\n                                                            [value]=\"100\"\r\n                                                            [template]=\"'success'\">\r\n                                            </jqxProgressBar>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-md-2\">\r\n                                <div style=\"margin-left: 125px; float: left\">\r\n                                    <jqxGauge *ngIf=\"humidityEnabled === true\" #humid1\r\n                                              [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                              [ranges]=\"humidRanges\" [ticksMinor]=\"humiTicksMinor\" [ticksMajor]=\"humidTicksMajor\"\r\n                                              [labels]=\"humidLabels\" [style]=\"humidStyle\" [caption]=\"humidCaption\"\r\n                                              [cap]=\"'radius: 0.04'\" [width]=\"150\" [height]=\"150\" [max]=\"humidMax\" [min]=\"humidMin\">\r\n                                    </jqxGauge>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row avg-height\">\r\n                    <div #room2 class=\"container light-off walls\">\r\n                        <div class=\"row\" style=\"margin-left: 10px\">\r\n                            <p style=\"font-weight: bold\">Master Bedroom</p>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-2\">\r\n                                <div class=\"row\">\r\n                                    <div style=\"margin-left: 25px; float: left\">\r\n                                        <jqxGauge *ngIf=\"temperatureEnabled === true\" #temp2\r\n                                                  [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                                  [ranges]=\"tempRanges\" [ticksMinor]=\"tempTicksMinor\" [ticksMajor]=\"tempTicksMajor\"\r\n                                                  [labels]=\"tempLabels\" [style]=\"tempStyle\" [caption]=\"tempCaption\" [cap]=\"'radius: 0.04'\"\r\n                                                  [width]=\"150\" [height]=\"150\" [max]=\"tempMax\" [min]=\"tempMin\">\r\n                                        </jqxGauge>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 75px; margin-top: 25px; float: left\">\r\n                                            <p *ngIf=\"windowsEnabled === true\" style=\"font-weight: bold\">Windows opened</p>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 175px; margin-top: 25px; float: left\">\r\n                                            <jqxProgressBar *ngIf=\"windowsEnabled === true\" #windows2\r\n                                                            [animationDuration]=\"0\"\r\n                                                            [width]=\"50\"\r\n                                                            [height]=\"50\"\r\n                                                            [value]=\"100\"\r\n                                                            [template]=\"'success'\">\r\n                                            </jqxProgressBar>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-md-2\">\r\n                                <div style=\"margin-left: 125px; float: left\">\r\n                                    <jqxGauge *ngIf=\"humidityEnabled === true\" #humid2\r\n                                              [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                              [ranges]=\"humidRanges\" [ticksMinor]=\"humiTicksMinor\" [ticksMajor]=\"humidTicksMajor\"\r\n                                              [labels]=\"humidLabels\" [style]=\"humidStyle\" [caption]=\"humidCaption\"\r\n                                              [cap]=\"'radius: 0.04'\" [width]=\"150\" [height]=\"150\" [max]=\"humidMax\" [min]=\"humidMin\">\r\n                                    </jqxGauge>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3 border-left border-top border-bottom col-height\">\r\n                <div class=\"row border-bottom small-height\">\r\n                    <div #room3 class=\"container light-off walls\">\r\n                        <div class=\"row\" style=\"margin-left: 10px\">\r\n                            <p style=\"font-weight: bold\">Hallway</p>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-2\">\r\n                                <div class=\"row\">\r\n                                    <div style=\"margin-left: 25px; float: left\">\r\n                                        <jqxGauge *ngIf=\"temperatureEnabled === true\" #temp3\r\n                                                  [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                                  [ranges]=\"tempRanges\" [ticksMinor]=\"tempTicksMinor\" [ticksMajor]=\"tempTicksMajor\"\r\n                                                  [labels]=\"tempLabels\" [style]=\"tempStyle\" [caption]=\"tempCaption\" [cap]=\"'radius: 0.04'\"\r\n                                                  [width]=\"150\" [height]=\"150\" [max]=\"tempMax\" [min]=\"tempMin\">\r\n                                        </jqxGauge>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-md-2\">\r\n                                <div style=\"margin-left: 125px; float: left\">\r\n                                    <jqxGauge *ngIf=\"humidityEnabled === true\" #humid3\r\n                                              [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                              [ranges]=\"humidRanges\" [ticksMinor]=\"humiTicksMinor\" [ticksMajor]=\"humidTicksMajor\"\r\n                                              [labels]=\"humidLabels\" [style]=\"humidStyle\" [caption]=\"humidCaption\"\r\n                                              [cap]=\"'radius: 0.04'\" [width]=\"150\" [height]=\"150\" [max]=\"humidMax\" [min]=\"humidMin\">\r\n                                    </jqxGauge>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row large-height\">\r\n                    <div #room4 class=\"container light-off walls\">\r\n                        <div class=\"row\" style=\"margin-left: 10px\">\r\n                            <p style=\"font-weight: bold\">Living Room</p>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-2\">\r\n                                <div class=\"row\">\r\n                                    <div style=\"margin-left: 25px; float: left\">\r\n                                        <jqxGauge *ngIf=\"temperatureEnabled === true\" #temp4\r\n                                                  [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                                  [ranges]=\"tempRanges\" [ticksMinor]=\"tempTicksMinor\" [ticksMajor]=\"tempTicksMajor\"\r\n                                                  [labels]=\"tempLabels\" [style]=\"tempStyle\" [caption]=\"tempCaption\" [cap]=\"'radius: 0.04'\"\r\n                                                  [width]=\"150\" [height]=\"150\" [max]=\"tempMax\" [min]=\"tempMin\">\r\n                                        </jqxGauge>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 75px; margin-top: 25px; float: left\">\r\n                                            <p *ngIf=\"windowsEnabled === true\" style=\"font-weight: bold\">Windows opened</p>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 175px; margin-top: 25px; float: left\">\r\n                                            <jqxProgressBar *ngIf=\"windowsEnabled === true\" #windows4\r\n                                                            [animationDuration]=\"0\"\r\n                                                            [width]=\"50\"\r\n                                                            [height]=\"50\"\r\n                                                            [value]=\"100\"\r\n                                                            [template]=\"'success'\">\r\n                                            </jqxProgressBar>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-md-2\">\r\n                                <div style=\"margin-left: 125px; float: left\">\r\n                                    <jqxGauge *ngIf=\"humidityEnabled === true\" #humid4\r\n                                              [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                              [ranges]=\"humidRanges\" [ticksMinor]=\"humiTicksMinor\" [ticksMajor]=\"humidTicksMajor\"\r\n                                              [labels]=\"humidLabels\" [style]=\"humidStyle\" [caption]=\"humidCaption\"\r\n                                              [cap]=\"'radius: 0.04'\" [width]=\"150\" [height]=\"150\" [max]=\"humidMax\" [min]=\"humidMin\">\r\n                                    </jqxGauge>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3 border col-height\">\r\n                <div class=\"row border-bottom avg-height\">\r\n                    <div #room5 class=\"container light-off walls\">\r\n                        <div class=\"row\" style=\"margin-left: 10px\">\r\n                            <p style=\"font-weight: bold\">Kitchen</p>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-2\">\r\n                                <div class=\"row\">\r\n                                    <div style=\"margin-left: 25px; float: left\">\r\n                                        <jqxGauge *ngIf=\"temperatureEnabled === true\" #temp5\r\n                                                  [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                                  [ranges]=\"tempRanges\" [ticksMinor]=\"tempTicksMinor\" [ticksMajor]=\"tempTicksMajor\"\r\n                                                  [labels]=\"tempLabels\" [style]=\"tempStyle\" [caption]=\"tempCaption\" [cap]=\"'radius: 0.04'\"\r\n                                                  [width]=\"150\" [height]=\"150\" [max]=\"tempMax\" [min]=\"tempMin\">\r\n                                        </jqxGauge>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 75px; margin-top: 25px; float: left\">\r\n                                            <p *ngIf=\"windowsEnabled === true\" style=\"font-weight: bold\">Windows opened</p>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 175px; margin-top: 25px; float: left\">\r\n                                            <jqxProgressBar *ngIf=\"windowsEnabled === true\" #windows5\r\n                                                            [animationDuration]=\"0\"\r\n                                                            [width]=\"50\"\r\n                                                            [height]=\"50\"\r\n                                                            [value]=\"100\"\r\n                                                            [template]=\"'success'\">\r\n                                            </jqxProgressBar>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-md-2\">\r\n                                <div style=\"margin-left: 125px; float: left\">\r\n                                    <jqxGauge *ngIf=\"humidityEnabled === true\" #humid5\r\n                                              [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                              [ranges]=\"humidRanges\" [ticksMinor]=\"humiTicksMinor\" [ticksMajor]=\"humidTicksMajor\"\r\n                                              [labels]=\"humidLabels\" [style]=\"humidStyle\" [caption]=\"humidCaption\"\r\n                                              [cap]=\"'radius: 0.04'\" [width]=\"150\" [height]=\"150\" [max]=\"humidMax\" [min]=\"humidMin\">\r\n                                    </jqxGauge>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row avg-height\">\r\n                    <div #room6 class=\"container light-off walls\">\r\n                        <div class=\"row\" style=\"margin-left: 10px\">\r\n                            <p style=\"font-weight: bold\">Bathroom</p>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-2\">\r\n                                <div class=\"row\">\r\n                                    <div style=\"margin-left: 25px; float: left\">\r\n                                        <jqxGauge *ngIf=\"temperatureEnabled === true\" #temp6\r\n                                                  [value]=\"20\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                                  [ranges]=\"tempRanges\" [ticksMinor]=\"tempTicksMinor\" [ticksMajor]=\"tempTicksMajor\"\r\n                                                  [labels]=\"tempLabels\" [style]=\"tempStyle\" [caption]=\"tempCaption\" [cap]=\"'radius: 0.04'\"\r\n                                                  [width]=\"150\" [height]=\"150\" [max]=\"tempMax\" [min]=\"tempMin\">\r\n                                        </jqxGauge>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"row\">\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 75px; margin-top: 25px; float: left\">\r\n                                            <p *ngIf=\"windowsEnabled === true\" style=\"font-weight: bold\">Windows opened</p>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-md-2\">\r\n                                        <div style=\"margin-left: 175px; margin-top: 25px; float: left\">\r\n                                            <jqxProgressBar *ngIf=\"windowsEnabled === true\" #windows6\r\n                                                            [animationDuration]=\"0\"\r\n                                                            [width]=\"50\"\r\n                                                            [height]=\"50\"\r\n                                                            [value]=\"100\"\r\n                                                            [template]=\"'success'\">\r\n                                            </jqxProgressBar>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-md-2\">\r\n                                <div style=\"margin-left: 125px; float: left\">\r\n                                    <jqxGauge *ngIf=\"humidityEnabled === true\" #humid6\r\n                                              [value]=\"values[5]\" [colorScheme]=\"'scheme04'\" [animationDuration]=\"1500\"\r\n                                              [ranges]=\"humidRanges\" [ticksMinor]=\"humiTicksMinor\" [ticksMajor]=\"humidTicksMajor\"\r\n                                              [labels]=\"humidLabels\" [style]=\"humidStyle\" [caption]=\"humidCaption\"\r\n                                              [cap]=\"'radius: 0.04'\" [width]=\"150\" [height]=\"150\" [max]=\"humidMax\" [min]=\"humidMin\">\r\n                                    </jqxGauge>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__aspnet_signalr__ = __webpack_require__("./node_modules/@aspnet/signalr/dist/browser/signalr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__aspnet_signalr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__aspnet_signalr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__ = __webpack_require__("./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxgauge.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jqwidgets_scripts_jqwidgets_ts_angular_jqxprogressbar__ = __webpack_require__("./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxprogressbar.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__ = __webpack_require__("./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this._previousGroup = 'Any';
        this.title = 'SmartHouse';
        this.values = [];
        this.maxValue = 80;
        this.tempSensors = [];
        this.warnings = [];
        // Sensor group selection options
        this.groups = [
            'Any',
            'Temperature',
            'Humidity',
            'Light',
            'Windows'
        ];
        this.temperatureEnabled = true;
        this.humidityEnabled = true;
        this.lightEnabled = true;
        this.windowsEnabled = true;
        // Humidity widget configurations
        this.humidMin = 10;
        this.humidMax = 90;
        this.humidTicksMinor = { interval: 5, size: '5%' };
        this.humidTicksMajor = { interval: 10, size: '15%' };
        this.humidLabels = { visible: true, position: 'inside' };
        this.humidStyle = { stroke: '#ffffff', 'stroke-width': '1px', fill: '#ffffff' };
        this.humidCaption = { value: 'Humidity', position: 'bottom' };
        this.humidRanges = [
            { startValue: 10, endValue: 30, style: { fill: '#e2e2e2', stroke: '#e2e2e2' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
            { startValue: 30, endValue: 50, style: { fill: '#f6de54', stroke: '#f6de54' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
            { startValue: 50, endValue: 70, style: { fill: '#db5016', stroke: '#db5016' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
            { startValue: 70, endValue: 90, style: { fill: '#d02841', stroke: '#d02841' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 }
        ];
        // Temperature widget configurations
        this.tempMin = 15;
        this.tempMax = 35;
        this.tempTicksMinor = { interval: 1, size: '5%' };
        this.tempTicksMajor = { interval: 5, size: '15%' };
        this.tempLabels = { visible: true, position: 'inside', interval: 5 };
        this.tempStyle = { stroke: '#ffffff', 'stroke-width': '1px', fill: '#ffffff' };
        this.tempCaption = { value: 'Temperature', position: 'bottom' };
        this.tempRanges = [
            { startValue: 15, endValue: 20, style: { fill: '#e2e2e2', stroke: '#e2e2e2' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
            { startValue: 20, endValue: 25, style: { fill: '#f6de54', stroke: '#f6de54' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
            { startValue: 25, endValue: 30, style: { fill: '#db5016', stroke: '#db5016' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 },
            { startValue: 30, endValue: 35, style: { fill: '#d02841', stroke: '#d02841' }, startDistance: '5%', endDistance: '5%', endWidth: 13, startWidth: 13 }
        ];
    }
    // Called when view is initialized. Creates hub connection, subscribes to events
    // and starts the connection to enable communication.
    AppComponent.prototype.ngOnInit = function () {
        this._hubConnection = new __WEBPACK_IMPORTED_MODULE_1__aspnet_signalr__["HubConnection"]('/hubs/SmartHome');
        this.defineMethodsOnProxy();
        this.startConnection();
    };
    AppComponent.prototype.defineMethodsOnProxy = function () {
        var _this = this;
        this._hubConnection.on('SensorUpdated', function (location, name, reading) {
            var elem = _this[name];
            if (elem) {
                if (name.includes('light')) {
                    if (_this.lightEnabled) {
                        elem.nativeElement.className = reading == 'True' ? 'container light-on walls' : 'container light-off walls';
                    }
                }
                else if (name.includes('windows')) {
                    elem.val(reading == 'True' ? 100 : 0);
                }
                else {
                    elem.val(reading);
                }
            }
        });
        this._hubConnection.on('ShowWarning', function (warningMessage) {
            var warningSpan = document.getElementById('warning');
            warningSpan.innerText = warningMessage;
            _this.notification.open();
            _this.warnings.push(warningMessage);
        });
    };
    AppComponent.prototype.startConnection = function () {
        var _this = this;
        this._hubConnection.start()
            .then(function () {
            console.log('Hub connection started');
            _this._hubConnection.invoke('NotifyReadingsForAllSensors');
            _this._hubConnection.invoke('UpdateGroup', _this._previousGroup.toLowerCase(), _this._previousGroup.toLowerCase());
        })
            .catch(function () {
            console.log('Error while establishing connection');
        });
    };
    // Called when view is destroyed. Stops the hub connection. 
    AppComponent.prototype.ngOnDestroy = function () {
        this.stopConnection();
    };
    AppComponent.prototype.stopConnection = function () {
        this._hubConnection.stop()
            .then(function () {
            console.log('Hub connection stopped');
        })
            .catch(function () {
            console.log('Error while closing connection');
        });
    };
    // Handler for changing sensor group selection. Sends an UpdateGroup message to hub.
    AppComponent.prototype.onChange = function (group) {
        console.log(group);
        var previousLightEnabled = this.lightEnabled;
        this.temperatureEnabled = group === 'Any' || group === 'Temperature';
        this.humidityEnabled = group === 'Any' || group === 'Humidity';
        this.lightEnabled = group === 'Any' || group === 'Light';
        this.windowsEnabled = group === 'Any' || group === 'Windows';
        if (!this.lightEnabled) {
            this.setLights('container light-disabled walls');
        }
        else if (!previousLightEnabled) {
            this.setLights('container light-off walls');
        }
        this._hubConnection.invoke('UpdateGroup', group.toLowerCase(), this._previousGroup.toLowerCase());
        this._previousGroup = group;
    };
    AppComponent.prototype.setLights = function (lightDisabledClass) {
        this.setLight(this.light1, lightDisabledClass);
        this.setLight(this.light2, lightDisabledClass);
        this.setLight(this.light3, lightDisabledClass);
        this.setLight(this.light4, lightDisabledClass);
        this.setLight(this.light5, lightDisabledClass);
        this.setLight(this.light6, lightDisabledClass);
    };
    AppComponent.prototype.setLight = function (elem, className) {
        elem.nativeElement.className = className;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('notification'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__["a" /* jqxNotificationComponent */])
    ], AppComponent.prototype, "notification", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('temp1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "temp1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('temp2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "temp2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('temp3'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "temp3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('temp4'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "temp4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('temp5'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "temp5", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('temp6'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "temp6", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('windows1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_jqwidgets_scripts_jqwidgets_ts_angular_jqxprogressbar__["a" /* jqxProgressBarComponent */])
    ], AppComponent.prototype, "windows1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('windows2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_jqwidgets_scripts_jqwidgets_ts_angular_jqxprogressbar__["a" /* jqxProgressBarComponent */])
    ], AppComponent.prototype, "windows2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('windows3'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_jqwidgets_scripts_jqwidgets_ts_angular_jqxprogressbar__["a" /* jqxProgressBarComponent */])
    ], AppComponent.prototype, "windows3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('windows4'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_jqwidgets_scripts_jqwidgets_ts_angular_jqxprogressbar__["a" /* jqxProgressBarComponent */])
    ], AppComponent.prototype, "windows4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('windows5'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_jqwidgets_scripts_jqwidgets_ts_angular_jqxprogressbar__["a" /* jqxProgressBarComponent */])
    ], AppComponent.prototype, "windows5", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('windows6'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_jqwidgets_scripts_jqwidgets_ts_angular_jqxprogressbar__["a" /* jqxProgressBarComponent */])
    ], AppComponent.prototype, "windows6", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('humid1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "humid1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('humid2'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "humid2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('humid3'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "humid3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('humid4'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "humid4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('humid5'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "humid5", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('humid6'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */])
    ], AppComponent.prototype, "humid6", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('room1'),
        __metadata("design:type", HTMLDivElement)
    ], AppComponent.prototype, "light1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('room2'),
        __metadata("design:type", HTMLDivElement)
    ], AppComponent.prototype, "light2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('room3'),
        __metadata("design:type", HTMLDivElement)
    ], AppComponent.prototype, "light3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('room4'),
        __metadata("design:type", HTMLDivElement)
    ], AppComponent.prototype, "light4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('room5'),
        __metadata("design:type", HTMLDivElement)
    ], AppComponent.prototype, "light5", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* ViewChild */])('room6'),
        __metadata("design:type", HTMLDivElement)
    ], AppComponent.prototype, "light6", void 0);
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxbargauge__ = __webpack_require__("./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxbargauge.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__ = __webpack_require__("./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxgauge.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jqwidgets_scripts_jqwidgets_ts_angular_jqxlineargauge__ = __webpack_require__("./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxlineargauge.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jqwidgets_scripts_jqwidgets_ts_angular_jqxprogressbar__ = __webpack_require__("./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxprogressbar.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__ = __webpack_require__("./node_modules/jqwidgets-scripts/jqwidgets-ts/angular_jqxnotification.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_2_jqwidgets_scripts_jqwidgets_ts_angular_jqxbargauge__["a" /* jqxBarGaugeComponent */],
                __WEBPACK_IMPORTED_MODULE_3_jqwidgets_scripts_jqwidgets_ts_angular_jqxgauge__["a" /* jqxGaugeComponent */],
                __WEBPACK_IMPORTED_MODULE_4_jqwidgets_scripts_jqwidgets_ts_angular_jqxlineargauge__["a" /* jqxLinearGaugeComponent */],
                __WEBPACK_IMPORTED_MODULE_5_jqwidgets_scripts_jqwidgets_ts_angular_jqxprogressbar__["a" /* jqxProgressBarComponent */],
                __WEBPACK_IMPORTED_MODULE_6_jqwidgets_scripts_jqwidgets_ts_angular_jqxnotification__["a" /* jqxNotificationComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_http__["a" /* HttpModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map